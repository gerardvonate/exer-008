package org.example
import com.opencsv.CSVWriter
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.FileOutputStream
import java.io.FileWriter
import java.lang.String.valueOf
import java.sql.DriverManager
import java.sql.ResultSet

fun main() {

    val agreementSQL = "SELECT * FROM agreement.agreement"
    val signatorySQL = "SELECT * FROM agreement.signatory"
    var exit = false
    while(!exit) {
        println("EXER-008")
        println("1 - Export CSV of agreements table")
        println("2 - Export XLSX of agreements table")
        println("3 - Export CSV of signatory table")
        println("4 - Export XLSX of signatory table")
        val input = Integer.parseInt(readln())
        when(input) {
            1 -> exportCSVFile(agreementSQL, "agreements")
            2 -> exportXLSXFile(agreementSQL, "agreements")
            3 -> exportCSVFile(signatorySQL, "signatory")
            4 -> exportXLSXFile(signatorySQL, "signatory")
            else -> {
                exit = true
            }
        }
    }

}

fun getSQLResultSet(queryString: String): ResultSet {
    val jdbcUrl = "jdbc:postgresql://rds.dev.corporate.lexagle.com:5432/corporate"
    val username = "lexadmin"
    val password = "BestProductEver!23"

    val connection = DriverManager.getConnection(jdbcUrl, username, password)
    val result = connection.prepareStatement(queryString).executeQuery()
    return result
}

fun exportXLSXFile(queryString: String, fileName: String) {
    val result = getSQLResultSet(queryString)
    var counter = 0
    val columnCount = result.metaData.columnCount

    val workbook = XSSFWorkbook()
    val sheet = workbook.createSheet("EXER")

    while(result.next()) {
        val row = sheet.createRow(counter++)
        for(i in 1..columnCount) {
            var cell = row.createCell(i - 1)
            val value = valueOf(result.getObject(i))
            cell.setCellValue(value)
        }
    }

    val outputStream = FileOutputStream("$fileName.xlsx")
    workbook.write(outputStream)
    workbook.close()
}

fun exportCSVFile(queryString: String, fileName: String) {
    val result = getSQLResultSet(queryString)
    val csvWriter = CSVWriter(FileWriter("$fileName.csv"))

    val columns = result.metaData.columnCount
    while(result.next()) {
        val currentRow = arrayOfNulls<String>(columns)
        for(i in 1..columns) {
            currentRow[i - 1] = valueOf(result.getObject(i))
        }
        csvWriter.writeNext(currentRow)
    }
    csvWriter.close()
}